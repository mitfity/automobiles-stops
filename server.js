global.__base = __dirname + '/';

var express = require("express"),
    api = require("./api/routes"),
    app = express();

app.use('/public', express.static(__dirname + '/public'));

app.use("/api", api);

app.use("/", function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

var port = process.env.PORT || 3000;

app.listen(port, function () {
    console.log("Listening on " + port);
});