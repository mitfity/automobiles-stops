/**
 * Created by mitfity on 09.05.2016.
 */

var _ = require('underscore');

module.exports = {

    _: _,

    /**
     * Sends http response with text and code passed via params.
     * @param {object} res  Express response object
     * @param {string} text Text to show in response
     * @param {number} code Response http code
     */
    sendResponse: function (res, text, code) {
        res.setHeader('Content-Type', 'text/html');
        res.writeHead(code || 200);
        res.write(text || '');
        res.end();
    },

    /**
     * Makes first latter of passed string capitalized.
     * @param   {string}   string String to capitalize first latter
     * @returns {string} String with capitalized first latter
     */
    capitalizeFirstLetter: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

};