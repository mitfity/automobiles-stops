var common = require(__base + 'api/common/functions'),
    routeController = require(__base + 'api/root/RouteController'),
    dbConnection =  require(__base + 'api/root/DBConnection');

var AutomobilesController = {

    /**
     * Method is called when GET method is passed without 'id' parameter.
     * Returns all data from the database table.
     * @param {object} req Express request
     * @param {object} res Express response
     */
    apiGetAll: function (req, res) {
        dbConnection.getConnection(function (err, connection) {
            connection.query("select * from automobile", function(err, rows) {
                res.json(rows);
                connection.release();
            });
        })
    },

    /**
     * Method is called when POST method is passed.
     * It checks if 'id' and other needed parameters are passed through http
     * body.
     * Updates data in database.
     * @param {object} req Express request
     * @param {object} res Express response
     */
    apiPost: function (req, res) {
        var automobileId = req.body.automobileId,
            latitude = req.body.latitude,
            longtitude = req.body.longtitude;
        
        if (!automobileId && automobileId !== 0) {
            common.sendResponse(res, 'Automobile ID needed.', 422);
            return;
        } else if (!latitude && latitude !== 0) {
            common.sendResponse(res, 'Latitude needed.', 422);
            return;
        } else if (!longtitude && longtitude !== 0) {
            common.sendResponse(res, 'Longtitude needed.', 422);
            return;
        }
        
        dbConnection.getConnection(function (err, connection) {
            connection.query("update automobile set ? where ?", 
                             [{ 
                                 latitude: latitude,
                                 longtitude: longtitude
                             }, { 
                                 automobileId: automobileId 
                             }], function(err, rows) {
                res.json(rows);
                connection.release();
            });
        })
    }
};

common._.extend(AutomobilesController, routeController);

module.exports = AutomobilesController;