/**
 * Created by mitfity on 09.05.2016.
 */
var common = require(__base + 'api/common/functions'),
    routeController = require(__base + 'api/root/RouteController'),
    dbConnection =  require(__base + 'api/root/DBConnection');

var StopsController = {

    /**
     * Method is called when GET request withoud 'id' parameter is passed.
     * Returns all data from the database table.
     * @param {object} req Express request
     * @param {object} res Express response
     */
    apiGetAll: function (req, res) {
        dbConnection.getConnection(function (err, connection) {
            connection.query("select * from stop", function(err, rows) {
                res.json(rows);
                connection.release();
            });
        })
    }
};

common._.extend(StopsController, routeController);

module.exports = StopsController;