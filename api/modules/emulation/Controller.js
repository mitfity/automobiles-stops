var common = require(__base + 'api/common/functions'),
    routeController = require(__base + 'api/root/RouteController'),
    fs = require('fs');

var EmulationController = {

    /**
     * Method is called when GET method is passed without 'id' parameter.
     * Returns coordinates array from ./coordinates.json file.
     * @param {object} req Express request
     * @param {object} res Express response
     */
    apiGetAll: function (req, res) {
        var coordinates = null;
        
        try {
            fs.readFile(__dirname + '/coordinates.json', 'utf8', function (err, data) {
                coordinates = JSON.parse(data);
                res.json(coordinates);
            });
        } catch(err) {
            common.sendResponse(res, err.stack, 500);
        }        
    }
};

common._.extend(EmulationController, routeController);

module.exports = EmulationController;