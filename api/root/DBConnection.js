/**
 * Created by mitfity on 09.05.2016.
 */
var mysql = require('mysql');

var DBConnection = {

    pool: null,

    /**
     * Initializes mysql connection configs and creates pool.
     */
    initialize: function () {
        this.pool = mysql.createPool({
            host     : 'localhost',
            user     : 'root',
            password : '123qwe',
            database : 'automobiles_stops'
        });
    },

    /**
     * Returns mysql connection from pool.
     * @param {function} callback
     */
    getConnection: function(callback) {
        this.pool.getConnection(function(err, connection) {
            callback(err, connection);
        });
    }
};

DBConnection.initialize();

module.exports = DBConnection;