/**
 * Created by mitfity on 09.05.2016.
 */
var common = require(__base + 'api/common/functions');

var RouteController = {

    /**
     * Processes route and calls required method.
     * Builds method name according to http method and 'id' parameter.
     * @param {object} req Express request
     * @param {object} res Express response
     */
    processRoute: function (req, res) {
        var methodName = 'api' + common.capitalizeFirstLetter(req.method.toLowerCase());

        // Split get single and get all requests
        if (req.method === 'GET' && !req.query.id) {
            methodName = methodName + 'All';
        }

        if (typeof this[methodName] === 'function') {
            this[methodName](req, res);
        } else {
            common.sendResponse(res, 'Route Controller method not found.', 404);
        }
    }
};

module.exports = RouteController;