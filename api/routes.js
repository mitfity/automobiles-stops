/**
 * Created by mitfity on 09.05.2016.
 */
var express = require('express'),
    router = express.Router(),
    url = require('url'),
    common = require(__base + 'api/common/functions'),
    bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.all('/*', function(req, res) {
    var pathname = url.parse(req.url).pathname,
        routeModuleController = null;

    try {
        routeModuleController = require('./modules/' + pathname + '/Controller');

        if (typeof routeModuleController.processRoute === 'function') {
            routeModuleController.processRoute(req, res);
        } else {
            common.sendResponse(res, 'Requested route controller not found.', 404);
        }
    } catch (e) {
        if (e.code === 'MODULE_NOT_FOUND') {
            common.sendResponse(res, 'Requested route not found.', 404);
        } else {
            common.sendResponse(res, 'Server error: ' + e.stack, 500);
        }
    }
});

module.exports = router;