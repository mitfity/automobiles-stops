class MapManager {
    
    constructor(config) {
        let renderTo = config.renderTo || document.body;
        
        this.map = new google.maps.Map(renderTo);
        this.centerMarker = null;
        this.infoWindow = null;
        this.markers = [];
        this.iconBase = '/public/style/img/';
    }
    
    /**
     * Sets center of the map.
     * @param {number} lat latitude
     * @param {number} lng longtitude
     */
    setCenter(lat, lng) {
        let latitude = lat || 0,
            longtitude = lng || 0;
        
        this.map.setCenter(new google.maps.LatLng(latitude, longtitude));
    }
    
    /**
     * Sets zoom of the map.
     * @param {number} zoom
     */
    setZoom(zoom) {
        if (zoom == null) {
            zoom = 15;
        }
        
        this.map.setZoom(zoom);
    }
    
    /**
     * Sets marker on map center.
     * @param {object} config Example:
     *                        {
     *                          title: 'CityCenter'
     *                        }
     */
    setCenterMarker(config) {
        if (this.centerMarker) {            
            this.centerMarker.setMap(null);
            google.maps.event.clearInstanceListeners(this.centerMarker);
            this.centerMarker = null;        
        }
        
        let map = this.map,
            center = map.getCenter();              

        this.centerMarker = new google.maps.Marker({
            position: center,
            map,
            title: config.title || 'Center'
        });        
    }
    
    /**
     * Adds marker with automobile icon to the map.
     * @param {object} config Example:
     *                        {
     *                          lat: 123.1
     *                          lng: -123.1
     *                        }
     */
    addAutomobileMarker(config) {
        let iconUrl = this.iconBase + 'car.png',
            map = this.map;
        
        let marker = new google.maps.Marker({
            position: {
                lat: config.lat,
                lng: config.lng
            },
            icon: iconUrl,
            map
        });
        
        this.markers.push(marker);
    }
    
    /**
     * Removes markers from the map. Except center marker.
     */
    clearMarkers() {
        let markers = this.markers;
        
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        
        markers = [];
    }
}

export default MapManager