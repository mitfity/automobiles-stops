/**
 * Created by mitfity on 10.05.2016.
 */

import axios from 'axios'
    
let singleton = Symbol();
let singletonEnforcer = Symbol()
    
class AutomobilesEmulator {
    
    constructor(enforcer) {
        if (enforcer !== singletonEnforcer) {
            throw "Cannot construct singleton";
        }
        
        this.coordinatesArray = null;
        this.automobiles = null;
        this.isEmulating = false;
    }    
    
    /**
     * Starts the emulation of automobiles.
     */
    emulate() {
        if (this.isEmulating) {
            return;
        }
        
        this.isEmulating = true;        
        
        this.fetchData(() => {
            this.update();
            setInterval(this.update.bind(this), 4000);
        })      
    }
    
    /**
     * Fetches emulation data from server and sets emulationIndex for autos.
     * @param {function} success Callback
     */
    fetchData(success) {
        this.fetchCoordinates(coordinatesArray => {
            this.coordinatesArray = coordinatesArray;
            
            this.fetchAutomobiles(automobiles => {
                this.automobiles = automobiles;
                // set starting emulation index for each auto
                this.automobiles.forEach((auto, index, coll) => {
                    let coordinates = this.coordinatesArray;                    
                    auto.emulationIndex = Math.floor( 
                        (coordinates.length / coll.length) 
                        * index
                    );
                });

                if (typeof success === 'function') {
                    success();
                }
            });  
        });        
    }
    
    /**
     * Fetches automobiles from server.
     * @param {function} success Callback
     */
    fetchAutomobiles(success) {
        axios.get('/api/automobiles').then(response => {            
            if (typeof success === 'function') {
                success(response.data);
            }
        });
    }
    
    /**
     * Fetches coordinates from server.
     * @param {function} success Callback
     */
    fetchCoordinates(success) {
        axios.get('/api/emulation').then(response => {            
            if (typeof success === 'function') {
                success(response.data);
            }
        });
    }
    
    /**
     * Sets new coordinates to automobile. Generates new index of coordinates
     * from emulation coordinates array.
     * @param {object} automobile
     */
    setNewCoordinates(automobile) {
        var oldIndex = automobile.emulationIndex,
            newIndex = oldIndex + 1;
        
        if (newIndex >= this.coordinatesArray.length) {
            newIndex = 0;
        }
        
        automobile.emulationIndex = newIndex;
        automobile.latitude = this.coordinatesArray[newIndex].latitude;
        automobile.longtitude = this.coordinatesArray[newIndex].longtitude;
    }
    
    /**
     * Posts new coordinates to server.
     */
    update() {
        this.automobiles.forEach((automobile, index, coll) => {
            this.setNewCoordinates(automobile);
            axios.post('/api/automobiles', automobile).then(response => {                
            });
        });
    }    
    
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new AutomobilesEmulator(singletonEnforcer);
        }
        return this[singleton];
    }
}

export default AutomobilesEmulator