/**
 * Created by mitfity on 10.05.2016.
 */

import axios from 'axios'
    
let singleton = Symbol();
let singletonEnforcer = Symbol()
    
class CoordinatesManager {
    
    constructor(enforcer) {
        if (enforcer !== singletonEnforcer) {
            throw "Cannot construct singleton";
        }
        
        this.stops = null;
        this.automobiles = null;
        this.coordinatesChangedListeners = [];
        
        this.update();
        setInterval(this.update.bind(this), 2000);
    }
    
    /**
     * Returns stops or fetches them from server.
     * @param {function} success Callback
     */
    getStops(success) {
        if (this.stops && this.stops.length) {
            if (typeof success === 'function') {
                success(this.stops);
                return;
            }
        }
        
        this.fetchStops(success);
    }
    
    
    /**
     * Returns automobiles or fetches them from server.
     * @param {function} success Callback
     */
    getAutomobiles(success) {
        if (this.automobiles && this.automobiles.length) {
            if (typeof success === 'function') {
                success(this.automobiles);
                return;
            }
        }
        
        this.fetchAutomobiles(success);
    }
    
    /**
     * Fetches stops from server.
     * @param {function} success Callback
     */
    fetchStops(success) {
        axios.get('/api/stops').then(response => { 
            let stops = response.data;
            
            stops.forEach((stop, index, coll) => {
                stop.automobiles = null;
                stop.isAutomobileNear = false;
            });
            
            if (typeof success === 'function') {
                success(stops);
            }
        });
    }    
    
    /**
     * Subscribes on coordinates changes.
     * @param   {function} callback Event handler
     * @returns {number} Listener id (is needed to unsubscribe)
     */
    onCoordinatesChanged(callback) {
        let listeners = this.coordinatesChangedListeners,
            id = listeners.length;
        
        listeners.push({
            id: id,
            callback: callback
        });
        
        return id;
    }
    
    /**
     * Unsubscribes on coordinates changes.
     * @param {number} listenedId
     */
    offCoordinatesChanged(listenedId) {
        let listeners = this.coordinatesChangedListeners;
        
        for (let i = 0; i < listeners.length; i++) {
            if (listeners[i].id === listenedId) { 
                listeners.splice(i, 1);
                break;
            }
        }
    }
    
    /**
     * Calls event handlers of all listeners.
     * @param {[[Type]]} stops [[Description]]
     */
    coordinatesChangedDispatch(stops) {
        let automobiles = this.automobiles;
        
        this.coordinatesChangedListeners.forEach((listener, index, col) => {
            if (typeof listener.callback === 'function') {
                listener.callback(stops, automobiles);
            }
        });
    }
    
    /**
     * Looks for coordinates changes on server and dispatches 
     * if there are changes.
     */
    update() {
        // Do nothing if there is no listeners
        if (!this.coordinatesChangedListeners 
            || !this.coordinatesChangedListeners.length) {
            return;
        }
        // Initialize stops array if needed
        if (!this.stops || !this.stops.length) {
            this.fetchStops((stops) => {
                this.stops = stops;
                
                // Wait some time to update again if there were no stops due to
                // server error or something else.
                if (!stops || !stops.length) {
                    setTimeout(this.update.bind(this), 2000);
                    return;
                }
                this.update();
            });
            return;
        }
        
        this.fetchAutomobiles(automobiles => {
            let areCoordinatesChanged = this.areCoordinatesChanged(automobiles);
            
            this.rememberPreviousCoordinates(automobiles);
            
            if (!areCoordinatesChanged) {
                return;
            }            
            
            this.findAutomobilesNearStops(automobiles, this.stops);
            this.coordinatesChangedDispatch(this.stops);
        });
    }
    
    /**
     * Checks if new automobiles coordinates differ from previous.
     * @param   {Array} automobiles New automobiles
     * @returns {boolean} True if coordinates changed
     */
    areCoordinatesChanged(automobiles) {
        if (!this.automobiles || !this.automobiles.length 
            || !automobiles || !automobiles.length) {
            return false;
        }
        
        for (let i = 0; i < automobiles.length; i++) {
            if (!this.automobiles[i]) {
                return true;
            }
            
            if (this.automobiles[i].latitude !== automobiles[i].latitude 
                || this.automobiles[i].longtitude !== automobiles[i].longtitude) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Sets previous coordinates to new automobiles.
     * @param {Array} automobiles New automobiles
     */
    rememberPreviousCoordinates(automobiles) {
        // Remember previous coordinates to find out direction
        if (this.automobiles) {
            for (let i = 0; i < automobiles.length; i++) {
                if (this.automobiles[i]) {
                    automobiles[i].previousLatitude 
                        = this.automobiles[i].latitude;
                    automobiles[i].previousLongtitude 
                        = this.automobiles[i].longtitude;
                }
            }
        }
        
        this.automobiles = automobiles;
    }
    
    /**
     * Fetches automobiles from server.
     * @param {function} success Callback
     */
    fetchAutomobiles(success) {
        axios.get('/api/automobiles').then(response => { 
            let automobiles = response.data;                     
            
            if (typeof success === 'function') {
                success(automobiles);
            }
        });
    }
    
    /**
     * Finds automobiles near each stop and adds them to stop.
     * @param {Array} automobiles
     * @param {Array} stops
     */
    findAutomobilesNearStops(automobiles, stops) {
        const minimumDistanceToStop = 500;
        
        stops.forEach((stop, index, coll) => {
            let lat = stop.latitude,
                long = stop.longtitude;
            
            stop.automobiles = [];
            
            automobiles.forEach((automobile, index, coll) => {
                let distanceToStop = 0,
                    autoLat = automobile.latitude,
                    autoLong = automobile.longtitude;
                
                distanceToStop = this.getDistanceFromLatLonInM(
                    lat, long, autoLat, autoLong);
                
                if (distanceToStop <= minimumDistanceToStop 
                    && 
                    this.isAutomobileMovingTowardsStop(automobile, 
                                                       stop, 
                                                       stops)) {
                    automobile.distanceToStop = distanceToStop;
                    stop.automobiles.push(automobile);
                }
            });
            
            stop.isAutomobileNear = stop.automobiles.length && true;
        });
    }
    
    /**
     * Finds out if automobiles is moving towards stop.
     * @param   {object}   automobile
     * @param   {object}   stop
     * @param   {Array} stops
     * @returns {boolean} True if automobiles is moving towards stop
     */
    isAutomobileMovingTowardsStop(automobile, stop, stops) {
        let oldDistanceToPreviousStop = 0,
            distanceToPreviousStop = 0,
            oldDistanceToStop = 0,
            distanceToStop = 0,
            previousStopId = 0,
            previousStop = null;
        
        if (stop.stopId - 1 <= 0) {
            previousStopId = stops[stops.length - 1].stopId;
        } else {
            previousStopId = stop.stopId - 1;
        }
        
        for (let i = 0; i < stops.length; i++) {
            if (stops[i].stopId === previousStopId) {
                previousStop = stops[i];
                break;
            }
        }
        
        distanceToPreviousStop = this.getDistanceFromLatLonInM(
            automobile.latitude, automobile.longtitude,
            previousStop.latitude, previousStop.longtitude
        );
        
        oldDistanceToPreviousStop = this.getDistanceFromLatLonInM(
            automobile.previousLatitude, automobile.previousLongtitude,
            previousStop.latitude, previousStop.longtitude
        );
        
        distanceToStop = this.getDistanceFromLatLonInM(
            automobile.latitude, automobile.longtitude,
            stop.latitude, stop.longtitude
        );
        
        oldDistanceToStop = this.getDistanceFromLatLonInM(
            automobile.previousLatitude, automobile.previousLongtitude,
            stop.latitude, stop.longtitude
        );
        
        
        return (oldDistanceToPreviousStop < distanceToPreviousStop) 
            && (distanceToStop < oldDistanceToStop);
    }
    
    /**
     * Calculates distance in meters from two lat/long coordinates
     * @param   {number} lat1
     * @param   {number} lon1
     * @param   {number} lat2
     * @param   {number} lon2 
     * @returns {number} Distance in meters
     */
    getDistanceFromLatLonInM(lat1, lon1, lat2, lon2) {
        let R = 6371; // Radius of the earth in km
        let dLat = this.deg2rad(lat2-lat1);
        let dLon = this.deg2rad(lon2-lon1); 
        let a = Math.sin(dLat/2) * Math.sin(dLat/2) 
            + Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) 
            * Math.sin(dLon/2) * Math.sin(dLon/2); 
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        let d = R * c * 1000;
        
        return d;
    }

    /**
     * Converts degrees to radians.
     * @param   {number} deg
     * @returns {number} radians
     */
    deg2rad(deg) {
        return deg * (Math.PI/180)
    }
    
    
    static get instance() {
        if (!this[singleton]) {
            this[singleton] = new CoordinatesManager(singletonEnforcer);
        }
        return this[singleton];
    }
}

export default CoordinatesManager