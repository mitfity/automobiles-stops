/**
 * Created by mitfity on 10.05.2016.
 */

import React from 'react'
import { render } from 'react-dom'
import { Router, Route, browserHistory } from 'react-router'
import AutomobilesStopsApp from './components/AutomobilesStopsApp'
import StopList from './components/stopList/StopList'
import StopDetailedViewIndex from './components/stopDetailedView/StopDetailedViewIndex'
import StopDetailedView from './components/stopDetailedView/StopDetailedView'

render((
        <Router history={browserHistory}>
            <Route component={AutomobilesStopsApp}>
                <Route path="/" component={StopList}/>
                <Route path="/stops" component={StopDetailedViewIndex}> 
                    <Route path=":id" component={StopDetailedView} />
                </Route>
            </Route>
        </Router>
    ), document.getElementById('main')
);