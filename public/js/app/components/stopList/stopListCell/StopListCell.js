/**
 * Created by mitfity on 10.05.2016.
 */

import React from 'react'
import { Link } from 'react-router'

class StopListCell extends React.Component {

    constructor(props) {
        super(props);
    }
    
    /**
     * Generates stop status text according if there is automobile near.
     * @returns {[[Type]]} [[Description]]
     */
    getStatusText() {
        if (this.props.isAutomobileNear) {
            return (
                <span className={"label label-success pull-right"}>
                    Automobile is coming
                </span>
            );
        }
        return (
            <span className={"label label-warning pull-right"}>
                No automobiles near
            </span>
        );
    }

    render() {
        return (
            <Link to={this.props.linkTo} className="list-group-item" >
                {this.props.title}
                {this.getStatusText()}
            </Link>
        )
    }
}

export default StopListCell