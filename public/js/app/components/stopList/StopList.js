/**
 * Created by mitfity on 10.05.2016.
 */

import React from 'react'
import StopListCell from './stopListCell/StopListCell'
import CoordinatesManager 
    from '~/submodules/coordinatesManager/CoordinatesManager'

class StopList extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            stops: null
        };
    }
    
    /**
     * Fetches stops and subscribes on coordinates changes.
     */
    componentWillMount() {
        let coordinatesManager = CoordinatesManager.instance;   
        
        coordinatesManager.fetchStops(stops => {
            this.setState({
                stops
            });
        });
        
        this.coordinatesChangedId = coordinatesManager
            .onCoordinatesChanged(this.onCoordinatesChanged.bind(this));
    }
    
    /**
     * Coordinates change handler.
     * @param {Array} stops
     */
    onCoordinatesChanged(stops) {
        this.setState({
            stops
        });
    }
    
    /**
     * Unsubscribes on coordinates changes.
     */
    componentWillUnmount() {
        let coordinatesManager = CoordinatesManager.instance;
        
        coordinatesManager
            .offCoordinatesChanged(this.coordinatesChangedId);
    }
    
    /**
     * Generates React html cell nodes.
     * @returns {[[Type]]} [[Description]]
     */
    getCells() {
        let cells = null;
        
        if (this.state.stops && this.state.stops.length) {
            cells = [];
            this.state.stops.forEach((stop, index, coll) => {
                cells.push((
                    <StopListCell title={stop.name} 
                        isAutomobileNear={stop.isAutomobileNear} 
                        key={'stop' + stop.stopId} 
                        linkTo={'/stops/' + stop.stopId} />
                ));
            });
        }
        
        return cells;
    }

    render() {
        return (
            <div className="stop-list container-fluid">
                <ul className="list-group">
                    {this.getCells()}
                </ul>
            </div>
        )
    }
}

export default StopList