/**
 * Created by root on 11.05.16.
 */

import React from 'react'

class StopDetailedViewIndex extends React.Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <div className="stop-detailed-view-container container">
                {this.props.children || (<h2>This is a stop detailed view index.</h2>)}
            </div>
        )
    }
}

export default StopDetailedViewIndex