/**
 * Created by mitfity on 10.05.2016.
 */

import React from 'react'
import CoordinatesManager 
    from '~/submodules/coordinatesManager/CoordinatesManager'
import AutomobilesMap from './automobilesMap/AutomobilesMap'
    
class StopDetailedView extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            stops: null,
            stop: null,
            automobiles: null
        };
        
        this.stopId = parseInt(this.props.params.id, 10);           
    }
    
    /**
     * Initializes state stops and stop. Subscribes on coordinates changes.
     */
    componentWillMount() {
        let coordinatesManager = CoordinatesManager.instance;   
        
        coordinatesManager.getStops(stops => {
            let stop = null,
                stopId = this.stopId;
            
            stop = this.getStopById(stopId, stops);            
            
            coordinatesManager.getAutomobiles(automobiles => {
                this.setState({
                    stops,
                    stop,
                    automobiles
                });
            });            
        });
        
        this.coordinatesChangedId = coordinatesManager
            .onCoordinatesChanged(this.onCoordinatesChanged.bind(this));     
    }
    
    /**
     * Updates stopId and stop due to new props.
     * @param {object} nextProps [[Description]]
     */
    componentWillReceiveProps(nextProps) {
        let newStopId = parseInt(nextProps.params.id, 10),
            newStop = null;
        
        if (this.stopId !== newStopId) {
            this.stopId = newStopId;
            
            newStop = this.getStopById(newStopId, this.state.stops);
            this.setState({
                stop: newStop
            });
        }
    }
    
    /**
     * Returns stop from stops due to passed stopId.
     * @param   {number} stopId
     * @param   {Array} stops
     * @returns {object} stop with passed stopId or null
     */
    getStopById(stopId, stops) {
        if (!stops || !stops.length) {
            return null;
        }
        
        for (let i = 0; i < stops.length; i++) {
            if (stops[i].stopId === stopId) {
                return stops[i];
            }
        }
        
        return null;
    }

    /**
     * Coordinates changed handler.
     * @param {[[Type]]} stops [[Description]]
     */
    onCoordinatesChanged(stops, automobiles) {
        let stop = null,
            stopId = this.stopId;
        
        stop = this.getStopById(stopId, stops);
        
        this.setState({
            stops,
            stop,
            automobiles
        });
    }
    
    /**
     * Unsubscribes on coordinates changes.
     */
    componentWillUnmount() {
        let coordinatesManager = CoordinatesManager.instance;
        
        coordinatesManager
            .offCoordinatesChanged(this.coordinatesChangedId);
    }
    
    /**
     * Generates stop status due to if there is automobile near.
     * @returns {[[Type]]} [[Description]]
     */
    getStatusNodes() {
        if (!this.state.stop) {
            return null;
        }
        
        if (!this.state.stop.isAutomobileNear) {
            return (
                <p className="bg-warning">
                    No automobiles near.
                </p>
            );
        }
        
        return (
            <p className="bg-success">
                Automobile is near.
            </p>
        );
    }
    
    /**
     * Generates automobile list of automobiles that are near stop.
     * @returns {object} React nodes
     */
    getAutomobileListNodes() {        
        if (!this.state.stop || !this.state.stop.automobiles 
           || !this.state.stop.automobiles.length) {
            return null;
        }
        
        let automobiles = this.state.stop.automobiles,
            liNodes = [];
        
        automobiles.forEach((automobile, index, coll) => {
            liNodes.push((
                <li key={'auto' + automobile.id} className="list-group-item">
                    {automobile.name}
                    <span className="text-success pull-right">m</span>
                    <span className="pull-right">
                        {automobile.distanceToStop.toFixed(2)}
                    </span>
                    <span className="text-muted pull-right">distance:&nbsp;</span>
                </li>
            ));
        });
        
        return (
            <ul className="list-group">
                {liNodes}
            </ul>
        );
    }
    
    render() {
        return (
            <div className="stop-detailed-view">
                <h2>{this.state.stop ? this.state.stop.name : 'Stop title'}</h2>
                {this.state.stop ? this.state.stop.latitude : '0.00'}
                <span className="text-muted">lat</span>
                &nbsp; : &nbsp;
                {this.state.stop ? this.state.stop.longtitude : '0.00'}
                <span className="text-muted">long</span>
                <hr />
                <AutomobilesMap stop={this.state.stop} 
                    automobiles={this.state.automobiles} />
                <hr />                
                {this.getStatusNodes()}
                {this.getAutomobileListNodes()}
            </div>
        )
    }
}

export default StopDetailedView