import React from 'react'
import ReactDOM from 'react-dom'
import MapManager from '~/submodules/mapManager/MapManager'
    
class AutomobilesMap extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            mapManager: null,
            didStopChange: false
        };
    }
    
    /**
     * Initializes mapManager if needed.
     */
    componentDidMount() {
        let renderTo, mapManager;
        
        mapManager = this.state.mapManager;        
        
        if (!mapManager) {
            renderTo = ReactDOM.findDOMNode(this);
            mapManager = new MapManager({
                renderTo
            });   
            
            this.setState({
                mapManager
            });
        }
        
        this.setState({
            didStopChange: true
        });
    }
    
    /**
     * Sets center/zoom of map to current stop and places center marker.
     */
    initCenterMarker() {
        let stop = this.props.stop,
            mapManager = this.state.mapManager;
        
        if (!stop || !mapManager) {
            return;
        }
        
        mapManager.setCenter(stop.latitude, stop.longtitude);
        mapManager.setZoom(15);
        mapManager.setCenterMarker({
            title: stop.name
        });
    }
    
    /**
     * Checks for stop change.
     * @param {object} nextProps
     */
    componentWillReceiveProps(nextProps) {
        let prevProps = this.props,
            didStopChange = false,
            prevStop, nextStop;
        
        if (prevProps && nextProps) {
            prevStop = prevProps.stop;
            nextStop = nextProps.stop;
            didStopChange = (prevStop == null && nextStop != null) 
                || (prevStop != null && nextStop == null)
                || prevStop.id !== nextStop.id 
                || prevStop.latitude !== nextStop.latitude 
                || prevStop.longtitude !== nextStop.longtitude;
        }
        
        this.setState({
            didStopChange
        });
    }
    
    /**
     * Updates automobile markers and center marker if needed.
     */
    componentDidUpdate() {
        let mapManager = this.state.mapManager,
            automobiles = this.props.automobiles,
            didStopChange = this.state.didStopChange;
        
        if (!mapManager) {
            return;
        }
        
        if (didStopChange) {
            this.initCenterMarker();
            didStopChange = false;
            this.setState({
                didStopChange
            });
        }
        
        mapManager.clearMarkers();
        if (!automobiles || !automobiles.length) {
            return;
        } 
        
        for (let i = 0; i < automobiles.length; i++) {
            mapManager.addAutomobileMarker({
                lat: automobiles[i].latitude,
                lng: automobiles[i].longtitude
            });
        }
    }
    
    render() {
        return (
            <div id="automobiles-map" className="automobiles-map">
            </div>
        )
    }    
}

export default AutomobilesMap