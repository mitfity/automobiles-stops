/**
 * Created by mitfity on 10.05.2016.
 */

import React from 'react'
import { Link } from 'react-router'
import AutomobilesEmulator 
    from '~/submodules/automobilesEmulator/AutomobilesEmulator'

class AutomobilesStopsApp extends React.Component {

    constructor(props) {
        super(props);
    }

    /**
     * Generates active class name of navigation link.
     * @param   {string} linkPath Link url
     * @returns {string} Class name ('active' or '')
     */
    getActiveClassName(linkPath) {
        let pathname = this.props.location.pathname,
            rootPath = '/',
            isActive = false;

        isActive = linkPath !== rootPath ? pathname.indexOf(linkPath) === 0
            : pathname === rootPath;

        return isActive ? 'active' : '';
    }
    
    /**
     * Calls emulator emulate method.
     * @param {object} e Event
     */
    emulate(e) {
        let emulator = AutomobilesEmulator.instance;
        emulator.emulate();
    }
    
    render() {
        return (
            <div className="automobiles-stops-app container">
                <h1>Automobiles Stops Tracker Application</h1>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="collapse navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li className={this.getActiveClassName('/')}>
                                    <Link to="/">Stop List</Link>
                                </li>
                                <li className={this.getActiveClassName('/stops')}>
                                    <Link to="/stops/1">Detailed View</Link>
                                </li>
                            </ul>
                            <div className="emulate-container pull-right">
                                <button className="btn btn-danger"
                                    onClick={this.emulate.bind(this)}>
                                    Emulate
                                </button>
                            </div>                            
                        </div>
                    </div>
                </nav>
                <div className="content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default AutomobilesStopsApp