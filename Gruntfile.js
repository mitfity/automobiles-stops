/**
 * Created by mitfity on 10.05.2016.
 */

module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserify: {
            options: {
                transform: [['babelify', { presets: ["react", "es2015"] }]]
            },
            files: {
                src: [
                    'public/js/app/**/*.js',
                    '!public/js/app/main.js',
                    'public/js/app/main.js'
                ],
                dest: 'public/js/<%= pkg.name %>.js'
            }
        },
        watch: {
            scripts: {
                files: ['<%= browserify.files.src %>'],
                tasks: ["browserify"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-browserify");
    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerTask('default', ['browserify']);
};